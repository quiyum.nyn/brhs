<?php
namespace App\user;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Registration extends Database
{
    public $id;
    public $name;
    public $f_h_name;
    public $mother_name;
    public $gender;
    public $passing_year;
    public $profession;
    public $contact;
    public $address;
    public $email;
    public $picture;
    public $religion;
    public $h_w_name;
    public $child_name;
    public $child_age;
    public $date;
    public $string_child_name;
    public $string_child_age;
    public $lastId;
    public $reference_id;
    public $ref_id;
    public $status;
    public $total_amount;


    public function setData($allPostData)
    {
        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("name", $allPostData)) {
            $this->name = $allPostData['name'];
        }

        if (array_key_exists("f_h_name", $allPostData)) {
            $this->f_h_name = $allPostData['f_h_name'];
        }
        if (array_key_exists("m_name", $allPostData)) {
            $this->mother_name = $allPostData['m_name'];
        }
        if (array_key_exists("gender", $allPostData)) {
            $this->gender = $allPostData['gender'];
        }
        if (array_key_exists("passing_year", $allPostData)) {
            $this->passing_year = $allPostData['passing_year'];
        }
        if (array_key_exists("profession", $allPostData)) {
            $this->profession = $allPostData['profession'];
        }
        if (array_key_exists("religion", $allPostData)) {
            $this->religion = $allPostData['religion'];
        }
        if (array_key_exists("address", $allPostData)) {
            $this->address = $allPostData['address'];
        }
        if (array_key_exists("contact", $allPostData)) {
            $this->contact = $allPostData['contact'];
        }
        if (array_key_exists("email", $allPostData)) {
            $this->email = $allPostData['email'];
        }
        if (array_key_exists("picture_name", $allPostData)) {
            $this->picture = $allPostData['picture_name'];
        }
        if (array_key_exists("h_w_name", $allPostData)) {
            $this->h_w_name = $allPostData['h_w_name'];
        }
        if (array_key_exists("child_name", $allPostData)) {
                $this->child_name = $allPostData['child_name'];
        }
        if (array_key_exists("child_age", $allPostData)) {
                $this->child_age = $allPostData['child_age'];
        }
        if (array_key_exists("reference_id", $allPostData)) {
            $this->reference_id = $allPostData['reference_id'];
        }
        if (array_key_exists("ref_id", $allPostData)) {
            $this->ref_id = $allPostData['ref_id'];
        }
        if (array_key_exists("total_fee", $allPostData)) {
            $this->total_amount = $allPostData['total_fee'];
        }
    }

    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $this->string_child_name=implode(',', $this->child_name);
        $this->string_child_age=implode(',', $this->child_age);
        $arrData=array($this->name,$this->f_h_name,$this->mother_name,$this->gender,$this->passing_year,$this->profession,$this->address,$this->contact,$this->email,$this->picture,$this->religion,$this->h_w_name,$this->string_child_name,$this->string_child_age,$this->date,$this->total_amount);

        $query= 'INSERT INTO registration (user_name,f_h_name,mother_name,gender,passing_year,profession,address,contact,email,picture,religion,h_w_name,child_name,child_age,registration_date,total_amount) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);
        $id = $this->DBH->lastInsertId();

        if($result){
            session_start();
            $_SESSION['reference_last_id']=$id;
        }
        Utility::redirect("../views/user/registration-step2.php");

    }
    public function storeManual(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $this->string_child_name=implode(',', $this->child_name);
        $this->string_child_age=implode(',', $this->child_age);
        $this->status="manual";
        $arrData=array($this->name,$this->f_h_name,$this->mother_name,$this->gender,$this->passing_year,$this->profession,$this->address,$this->contact,$this->email,$this->picture,$this->religion,$this->h_w_name,$this->string_child_name,$this->string_child_age,$this->date,$this->total_amount,$this->status);

        $query= 'INSERT INTO registration (user_name,f_h_name,mother_name,gender,passing_year,profession,address,contact,email,picture,religion,h_w_name,child_name,child_age,update_date,total_amount,status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);
        

        if($result){
             Utility::redirect("../views/admin/registeredMember.php"); 
        }

      

    }
    public function view($id){
        $sql = "SELECT * FROM registration WHERE id=".$id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showall(){
        $sql = "Select * from registration where status='incomplete'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function viewProfile(){
        $sql = "Select * from registration where id=".$this->reference_id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function approve(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $arrData  = array($this->date,"approved");
        $query= 'UPDATE registration SET update_date=?,status=? WHERE id='.$this->reference_id;
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Request has been approved successfully!");
        }
        else{
            Message::setMessage("Failed! Request has not been approved!");
        }
        Utility::redirect('../views/admin/memberRequest.php');
    }
    
     public function approveAgain(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $arrData  = array($this->date,"approved");
        $query= 'UPDATE registration SET update_date=?,status=? WHERE id='.$this->reference_id;
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Request has been approved successfully!");
        }
        else{
            Message::setMessage("Failed! Request has not been approved!");
        }
        Utility::redirect('../views/admin/rejectedMember.php');
    }
    public function reject(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $arrData  = array($this->date,"rejected");
        $query= 'UPDATE registration SET update_date=?,status=? WHERE id='.$this->reference_id;
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Request has been rejected successfully!");
        }
        else{
            Message::setMessage("Failed! Request has not been rejected!");
        }
        Utility::redirect('../views/admin/memberRequest.php');
    }
    public function rejectReg(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $arrData  = array($this->date,"rejected");
        $query= 'UPDATE registration SET update_date=?,status=? WHERE id='.$this->reference_id;
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Request has been rejected successfully!");
        }
        else{
            Message::setMessage("Failed! Request has not been rejected!");
        }
        Utility::redirect('../views/admin/registeredMember.php');
    }
    public function showallRegistared(){
        $sql = "Select * from registration where status IN ('approved','manual')";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showallRejected(){
        $sql = "Select * from registration where status='rejected'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function delete(){
        $query= "DELETE from registration WHERE id=".$this->reference_id;
        $result = $this->DBH->exec($query);
        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Delete!");
        }
        Utility::redirect('../views/admin/rejectedMember.php');
    }
    public function regStatus(){
        $sql = "Select * from registration WHERE id=".$this->ref_id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showallnumber(){
        $sql = "Select COUNT(status)as totalrow from registration where status='approved'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showallmanual(){
        $sql = "Select COUNT(status)as totalrow from registration where status='manual'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showallregistered(){
        $sql = "Select COUNT(status)as totalrow from registration where status IN ('approved','manual')";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    
    public function showallreq(){
        $sql = "Select COUNT(status)as totalrow from registration where status='incomplete'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function totalCollectedAmount(){
        $sql = "Select SUM(total_amount)as totaltaka from registration where status IN ('approved','manual')";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
}