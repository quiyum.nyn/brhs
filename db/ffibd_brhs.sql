-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 20, 2017 at 03:11 PM
-- Server version: 5.6.35-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ffibd_brhs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `id` int(22) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`id`, `name`, `email`, `password`, `status`) VALUES
(1, 'Md shahadat hossain babu', 'babu@ffibd.com', '3c34535c9c5bc690c503d4c93df37b01', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` int(11) NOT NULL,
  `user_name` varchar(256) NOT NULL,
  `f_h_name` varchar(256) NOT NULL,
  `mother_name` varchar(45) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `passing_year` varchar(11) NOT NULL,
  `profession` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `contact` varchar(23) NOT NULL,
  `email` varchar(200) NOT NULL,
  `picture` varchar(300) NOT NULL,
  `religion` varchar(24) NOT NULL,
  `h_w_name` varchar(256) NOT NULL,
  `child_name` varchar(256) NOT NULL,
  `child_age` varchar(50) NOT NULL,
  `registration_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `total_amount` varchar(24) NOT NULL,
  `status` varchar(22) NOT NULL DEFAULT 'incomplete'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `user_name`, `f_h_name`, `mother_name`, `gender`, `passing_year`, `profession`, `address`, `contact`, `email`, `picture`, `religion`, `h_w_name`, `child_name`, `child_age`, `registration_date`, `update_date`, `total_amount`, `status`) VALUES
(1256, 'Faruqul Islam', 'Md. Shah Alam', 'Alefa Begum', 'Male', '2006', 'BUSINESS', 'Road-2, house-20/D, Port Colony,Bandar', '01916806489', 'faruk9747@gmail.com', '1491668289Untitled.jpg', 'Islam', '', '', '', '2017-04-08 22:18:09', '2017-04-08 22:31:10', '1000', 'approved'),
(1275, 'Md. Rony Chowdhary', 'Md. Akhlasur Rahman', 'Asma Begum', 'Male', '2006', 'Student', 'road-7,house no-174/b, Port Colony', '01994104041', 'rrrony505@gmail.com', '1493307181Rony 001.jpg', 'Islam', '', '', '', '0000-00-00 00:00:00', '2017-04-27 21:33:01', '1000', 'manual'),
(1280, 'Shahadat Hossain Babu', 'Md. Amir Hossain', 'Kajal Rekha', 'Male', '2006', 'Student', 'road-7,house no-174/b, Port Colony', '01829820421', 'shahadat.ni7@gmail.com', '1493395181005.jpg', 'Islam', '', '', '', '0000-00-00 00:00:00', '2017-04-28 21:59:41', '1000', 'manual'),
(1282, 'm, sohel sarkar', 'late: m,a, mannan sarkar', 'mrs sultana razia sarkar', 'Male', '2000', 'poultry business', 'vill: chandiver (sth),h: no: 233,near by- red crescent, up: bhairab bazar, dis: kishoreghonj.', '01812802066', 'sohelsarkar786@yahoo.com', '1494684830', 'Islam', '', '', '', '2017-05-13 20:13:50', '2017-05-14 21:40:33', '1000', 'approved'),
(1287, 'Mohammad Amirul Haque', 'Mohammad Obidul haque', 'Akter Jahan', 'Male', '2007', 'P.Service', 'Block #J, Plot #55, Kaibullah dham,grahioyan, prakalpha,chittagong,Bangladesh.', '01670313136', 'amirul.city@gmail.com', '1502095993poney.jpg', 'Islam', '', '', '', '2017-08-07 14:53:13', '2017-08-19 15:20:53', '1000', 'rejected'),
(1288, 'Armin Akter', 'Sha Alam', 'Alefa Begum', 'Female', '2014', 'Student', 'House No-20/D, Road No-02, Port Colony, Bandar, Chittagong.', '01916806489', 'faruk9747@gmail.com', '1503115680IMG_20170505_134642.jpg', 'Islam', '', '', '', '2017-08-19 10:08:00', '2017-08-19 10:28:24', '1000', 'approved'),
(1289, 'Johirul Islam Polash', 'Md. Ruhul Amin', 'Khodeza Begum', 'Male', '2007', 'Pran RFL(ATSM)', 'Bissa colony,Firujsha,Akbarsha,Chittagong', '01670966676', '', '150313183017039236_1182279341889675_3720751873509347706_o.jpg', 'Islam', '', ',', ',', '2017-08-19 14:37:10', '2017-08-19 15:21:26', '1000', 'rejected'),
(1290, 'Johirul Islam Polash', 'Md. Ruhul Amin', 'Khodeza Begum', 'Male', '2007', 'Pran RFL(ATSM)', 'Bissa colony,Firujsha,Akbarsha,Chittagong', '01670966676', '', '150313198517039236_1182279341889675_3720751873509347706_o.jpg', 'Islam', '', '', '', '2017-08-19 14:39:45', '2017-08-19 15:22:00', '1000', 'approved'),
(1291, 'Rozina Akter', 'Md. Najim Uddin', 'Johora Bagum', 'Male', '2006', 'Housewife', 'House no-103/A,Road-5,Port colony', '01969484621', '', '1503133662010.jpg', 'Islam', '', '', '', '2017-08-19 15:07:42', '2017-08-19 15:22:15', '1000', 'approved'),
(1292, 'Razib Chandra Datta', 'Chaya Mohan Datta', 'Momata Datta', 'Male', '2006', 'Student', 'House No-418/A, Road No-13, Port Colony, Bandar, Chittagong.', '01623339899', '', '150313367134164.jpg', 'Hinduism', '', '', '', '2017-08-19 15:07:51', '2017-08-19 15:22:26', '1000', 'approved'),
(1293, 'Sozib Chandra Datta', 'Chaya Mohan Datta', 'Momata Datta', 'Male', '2006', 'Student', 'House No-418/A, Road No-13, Port Colony, Bandar, Chittagong.', '01832116646', '', '150313367733728...jpg', 'Hinduism', '', '', '', '2017-08-19 15:07:57', '2017-08-19 15:22:39', '1000', 'approved'),
(1294, 'F. M. Mizanur Rahaman', 'Md. Abdus Salam', 'Morium Sultana', 'Male', '2006', 'Private Service', 'House No-114/A, Road No-5, Port Colony, Bandar, Chittagong.', '01710479635', 'mizan03.cu@gmail.com', '15031338631014539_527988410570206_352084992_o.jpg', 'Islam', '', '', '', '2017-08-19 15:11:03', '2017-08-19 15:22:51', '1000', 'approved');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1295;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
