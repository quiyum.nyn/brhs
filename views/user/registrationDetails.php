<?php
require_once ("../../vendor/autoload.php");
session_start();
include ("../templateLayout/templateInformation.php");
use App\Authentication;
use App\Utility\Utility;
use App\user\Registration;
    $object= new Registration();
    $object->setData($_POST);
   
    $oneData=$object->regStatus();
    
if($oneData->h_w_name!="" && $oneData->child_name!="")
{
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$h_w_fee+$your_fee;
    $person=count($baby)+count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife & ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband & ".count($baby)." son/daughter";
    }
}
elseif ($oneData->h_w_name!=""){
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$h_w_fee+$your_fee;
    $person=count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband";
    }
}
elseif ($oneData->child_name!=""){
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$your_fee;
    $person=count($baby)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your ".count($baby)." son/daughter";
    }
}
else{
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$your_fee;
    $person=count($oneData->user_name);
    $info="";
}
if($person<=1) {
    $infoDetails=$person;
} else
{ $infoDetails= $person." (".$info.")";}
if($oneData->status=="approved"){
$regStatus="Registration has been completed";
}
elseif($oneData->status=="manual"){
$regStatus="Registration has been completed";
}
elseif($oneData->status=="rejected"){
$regStatus="Registration request has been rejected!";
}
elseif($oneData->status=="incomplete"){
$regStatus="Your registration is on processing!";
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <?php include ("../templateLayout/css/templateCss.php");?>
</head>
<body>
<div class="fh5co-loader"></div>
<div id="page">
    <?php include ("../templateLayout/navigation.php");?>
    <div id="fh5co-contact" class="fh5co-no-pd-top">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
                    <h2><span>Member Profile</span></h2><br>
                    <a href="../admin/adminPdf.php?reference_id=<?php echo $oneData->id?>" class="btn btn-info" role="button">Download copy as PDF</a>
                    <br><br>
                    <h3><?php echo $regStatus?></h3>
                </div>

            </div>
            <?php
            use App\Message\Message;
            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <p id='message' style='text-align: center; font-family: Pristina; font-size: 25px'>$msg</p>";

            }
            ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0"  style="margin-top:-60px">
                    <div class="form-group row">
                        <div class="col-md-12 field">
                            <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                <img src="../../resources/photos/<?php echo $oneData->picture?>" class="img-rounded img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <table class="table table-striped table-bordered">
                        <tbody>
                        <tr>
                            <td style="width: 40%">Member Name:</td>
                            <td><?php echo $oneData->user_name;?></td>
                        </tr>
                        <tr>
                            <td>Father/Husband's Name:</td>
                            <td><?php echo $oneData->f_h_name;?></td>
                        </tr>
                        <tr>
                            <td>Mother's Name:</td>
                            <td><?php echo $oneData->mother_name;?></td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td><?php echo $oneData->address;?></td>
                        </tr>
                        <tr>
                            <td>Contact:</td>
                            <td><?php echo $oneData->contact;?></td>
                        </tr>
                        <tr>
                            <td>E-mail:</td>
                            <td><?php echo $oneData->email;?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6 col-sm-6">
                    <table class="table table-striped table-bordered">
                        <tbody>
			<tr>
                            <td>Gender:</td>
                            <td><?php echo $oneData->gender;?></td>
                        </tr>
                        <tr>
                            <td style="width: 40%">Passing Year:</td>
                            <td><?php echo $oneData->passing_year;?></td>
                        </tr>
                        <tr>
                            <td>Religion:</td>
                            <td><?php echo $oneData->religion;?></td>
                        </tr>
                        <tr>
                            <td>Reference ID:</td>
                            <td><?php echo $oneData->id;?></td>
                        </tr>
                        <tr>
                            <td>Total Registered Person:</td>
                            <td><?php echo $infoDetails;?></td>
                        </tr>
                        <tr>
                            <td>Total Paid Amount:</td>
                            <td><?php echo $total_fee;?></td>
                        </tr>
                        </tbody>
                    </table>
                    
                </div>


            </div>

        </div>
    </div>
    <?php include ("../templateLayout/footer.php");?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<?php include ("../templateLayout/script/templateScript.php");?>
</body>
</html>