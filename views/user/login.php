<?php
session_start();
require_once("../../vendor/autoload.php");
require_once("../templateLayout/templateInformation.php");
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <?php include ("../templateLayout/css/templateCss.php");?>
</head>
<body>
<div class="fh5co-loader"></div>
<div id="page">
    <?php include ("../templateLayout/navigation.php");?>
    <div id="fh5co-contact" class="fh5co-no-pd-top">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
                    <h2><span>Admin Login</span></h2>
                </div>
            </div>
            <?php
            use App\Message\Message;
            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <p id='message' style='text-align: center; font-family: Pristina; font-size: 25px'>$msg</p>";

            }

            ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form action="../../controller/authentication/processLogin.php" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-12 field">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 field">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 field">
                                <input type="submit" id="submit" class="btn btn-primary" value="Login">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <?php include ("../templateLayout/footer.php");?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
<?php include ("../templateLayout/script/templateScript.php");?>
</body>
</html>