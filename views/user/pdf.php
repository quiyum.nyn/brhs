<?php
session_start();
require_once("../../vendor/autoload.php");
require_once("../templateLayout/templateInformation.php");


use App\user\Registration;
$object= new Registration();
$oneData=$object->view($_SESSION['reference_last_id']);
if($oneData->h_w_name!="" && $oneData->child_name!="")
{
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$h_w_fee+$your_fee;
    $person=count($baby)+count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife & ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband & ".count($baby)." son/daughter";
    }
}
elseif ($oneData->h_w_name!=""){
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$h_w_fee+$your_fee;
    $person=count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband";
    }
}
elseif ($oneData->child_name!=""){
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$your_fee;
    $person=count($baby)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your ".count($baby)." son/daughter";
    }
}
else{
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$your_fee;
    $person=count($oneData->user_name);
    $info="";
}
$ref_id=$_SESSION['reference_last_id'];
if($person<=1) {
    $infoDetails=$person;
} else
{ $infoDetails= $person." (".$info.")";}
$date=$oneData->registration_date;
$new=date('Y-m-d H:i:s',strtotime('+72 hour +0 minutes',strtotime($date)));
$newdate=date('d/m/Y h:i:s a', strtotime($new));
$html=<<<EOD
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>$title</title>
<!-- Animate.css -->
<link rel="stylesheet" href="../../resources/css/animate.css">

<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../resources/css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../resources/css/bootstrap.css">

<!-- Magnific Popup -->
<link rel="stylesheet" href="../../resources/css/magnific-popup.css">

<!-- Flexslider  -->
<link rel="stylesheet" href="../../resources/css/flexslider.css">
<link href="../../resources/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Theme style  -->
<link rel="stylesheet" href="../../resources/css/style.css">
<link rel="stylesheet" href="../../resources/css/step.css">

</head>
<body>
<div id="page">
    <div id="fh5co-contact" class="fh5co-no-pd-top">
        <div class="container">
            <div class="row animate-box" style="margin-top:-120px">
                <div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
                <img src="../../resources/photos/logo.png" width="100px">
                    <p style="text-align:center;font-size:22px">Reunion - 2017</p>
                    <p style="text-align:center;font-size:20px"><span>Bangladesh Railway Govt. High School, Saltgola, Chittagong.</span></p>
                    
                </div>
            </div>		
            <div class="row">
                <div class="col-md-12 col-md-offset-0 text-center">
                    <div class="form-group row" style="margin-top:-80px">
                        <div class="col-md-12 field">
                                <img src="../../resources/photos/$oneData->picture" width="160px" class="img-rounded img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                        <table class="table table-bordered" >
                        <tbody style="">
                            <tr>
                                <td style="width: 50%;font-size: 16px">Your Name</td>
                                <td style="width: 50%;font-size: 16px">$oneData->user_name</td>
                            </tr>
                            <tr>
                                <td style="width: 50%;font-size: 16px">Total Registered Person</td>
                               
                                <td style="width: 50%;font-size: 16px">$infoDetails</td>
                            </tr>
                            <tr>
                                <td style="width: 50%;font-size: 16px">Reference ID</td>
                                
                                <td style="width: 50%;font-size: 16px">$ref_id</td>
                            </tr>
                            <tr>
                                <td style="width: 50%;font-size: 16px">Total Payable Amount</td>
                                
                                <td style="width: 50%;font-size: 16px">$total_fee</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <h4 >Please follow these instruction</h4>
                    <h4>
                        <ol style="font-size:14px">
                            <li>Dial *322# from your mobile handset</li>
                            <li>Press 1 for payment option.</li>
                            <li>Press 1 for select bill pay option</li>
                            <li>Type "1" then press "Reply".</li>
                            <li>Enter Biller ID <b>"2310"</b></li>
                            <li>Enter Reference Number : <b>$ref_id</b> as your bill number</li>
                            <li>Enter amount <b>$total_fee</b></li>
                            <li>Enter your pin number</li>
                            <li>You will get a confirmation message.</li>
                        </ol>
                    </h4>
                    <p style="color: red;text-align:center">NB: Please pay your registration fee for reunion-2017 within 72 hours. </p>
                    <p style="color: green;text-align:center">Date-line: $newdate</p>
                </div>
             <hr>
               <p style="font-size: 16px;text-align:center">Technical Support: Future Features of IT. 156 CDA Avenue, East Nasirabad, Chittagong. website: www.ffibd.com</p>
            </div>
        </div>
    </div>
    
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
</body>
</html>
EOD;


$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output("$ref_id"."-".$oneData->user_name.".pdf",'D');