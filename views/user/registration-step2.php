<?php
session_start();
require_once("../../vendor/autoload.php");
require_once("../templateLayout/templateInformation.php");
use App\user\Registration;
$object= new Registration();
$oneData=$object->view($_SESSION['reference_last_id']);
if($oneData->h_w_name!="" && $oneData->child_name!="")
{
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$h_w_fee+$your_fee;
    $person=count($baby)+count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife & ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband & ".count($baby)." son/daughter";
    }
}
elseif ($oneData->h_w_name!=""){
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$h_w_fee+$your_fee;
    $person=count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband";
    }
}
elseif ($oneData->child_name!=""){
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$your_fee;
    $person=count($baby)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your ".count($baby)." son/daughter";
    }
}
else{
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$your_fee;
    $person=count($oneData->user_name);
    $info="";
}
if($person<=1) {
    $infoDetails=$person;
} else
{ $infoDetails= $person." (".$info.")";}
$date=$oneData->registration_date;
$new=date('Y-m-d H:i:s',strtotime('+72 hour +0 minutes',strtotime($date)));
$newdate=date('d/m/Y h:i:s a', strtotime($new));
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title;?></title>
    <?php include ("../templateLayout/css/templateCss.php");?>
</head>
<body>
<div class="fh5co-loader"></div>
<div id="page">
    <?php include ("../templateLayout/navigation.php");?>
    <div id="fh5co-contact" class="fh5co-no-pd-top">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
                    <h2><span>Registration</span></h2>
                    <br>
                    <a href="pdf.php" class="btn btn-info" role="button">Download your copy as PDF</a>
                </div>
            </div>

            <div class="stepwizard col-md-6 col-md-offset-3 col-xs-6 col-xs-offset-3">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#" type="button" class="btn btn-info btn-circle" disabled="disabled">1</a>
                        <p>Step 1</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#" type="button" class="btn btn-warning btn-circle">2</a>
                        <p>Step 2</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 col-xs-offset-0">
                    <div class="form-group row">
                        <div class="col-md-12 field">
                            <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                <img src="../../resources/photos/<?php echo $oneData->picture?>" class="img-rounded img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                        <div class="form-group row">
                            <div class="col-md-12 field">
                                <label>Your Name</label>
                                <input type="text" class="form-control" value="<?php echo $oneData->user_name;?>" readonly>
                            </div>
                        </div>
                    <div class="form-group row">
                        <div class="col-md-12 field">
                            <label>Total Registered Person</label>
                            <input type="text" class="form-control" value="<?php echo $infoDetails;?>" readonly>
                        </div>
                    </div>
                        <div class="form-group row">
                            <div class="col-md-12 field">
                                <label>Reference ID:</label>
                                <input type="text" value="<?php echo $_SESSION['reference_last_id'];?>" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 field">
                                <label>Total Payable Amount:</label>
                                <input type="text" value="<?php echo $total_fee;?>" class="form-control" readonly>
                            </div>
                        </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <hr>
                    <h4>Please follow these instruction</h4>
                    <hr>
                    <h4>
                        <ol style="line-height: 22px;">
                            <li>Dial *322# from your mobile handset</li>
                            <li>Press 1 for payment option.</li>
                            <li>Press 1 for select bill pay option</li>
                            <li>Type "1" then press "Reply".</li>
                            <li>Enter Biller ID <b>"2310"</b></li>
                            <li>Enter Reference Number : <b><?php echo $_SESSION['reference_last_id'];?></b> as your bill number</li>
                            <li>Enter amount <b><?php echo $total_fee?></b></li>
                            <li>Enter your pin number</li>
                            <li>You will get a confirmation message.</li>
                        </ol>
                        <p style="color: red;">NB: Please pay your registration fee for reunion-2017 within 72 hours. Date-line: <?php echo $newdate?> </p>
                    </h4>

                </div>
            </div>
            <a href="pdf.php?" class="btn btn-info" role="button">Download your copy as PDF</a>
        </div>
    </div>
    <?php include ("../templateLayout/footer.php");?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<?php include ("../templateLayout/script/templateScript.php");?>
</body>
</html>