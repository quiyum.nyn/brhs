<?php
require_once ("../../vendor/autoload.php");
session_start();
include ("../templateLayout/templateInformation.php");
use App\user\Registration;
$object= new Registration();
$object->setData($_GET);
$oneData=$object->viewProfile();
if($oneData->h_w_name!="" && $oneData->child_name!="")
{
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$h_w_fee+$your_fee;
    $person=count($baby)+count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife & ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband & ".count($baby)." son/daughter";
    }
}
elseif ($oneData->h_w_name!=""){
    $h_w_fee=(count($oneData->h_w_name)*1000);
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$h_w_fee+$your_fee;
    $person=count($oneData->h_w_name)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your wife";
    }
    elseif($oneData->gender=="Female"){
        $info="With your husband";
    }
}
elseif ($oneData->child_name!=""){
    $baby=explode(",",$oneData->child_name);
    $child_fee=count($baby)*800;
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$child_fee+$your_fee;
    $person=count($baby)+count($oneData->user_name);
    if($oneData->gender=="Male"){
        $info="With your ".count($baby)." son/daughter";
    }
    elseif($oneData->gender=="Female"){
        $info="With your ".count($baby)." son/daughter";
    }
}
else{
    $your_fee=(count($oneData->user_name)*1000);
    $total_fee=$your_fee;
    $person=count($oneData->user_name);
    $info="";
}
if($person<=1) {
    $infoDetails=$person;
} else
{ $infoDetails= $person." (".$info.")";}
if($oneData->status=="approved"){
$regStatus="Registration has been completed";
}
elseif($oneData->status=="manual"){
$regStatus="Registration has been completed";
}
elseif($oneData->status=="rejected"){
$regStatus="Registration request has been rejected!";
}
elseif($oneData->status=="incomplete"){
$regStatus="Your registration is on processing!";
}

$html=<<<EOD
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>$title</title>
<!-- Animate.css -->
<link rel="stylesheet" href="../../resources/css/animate.css">

<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../../resources/css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="../../resources/css/bootstrap.css">

<!-- Magnific Popup -->
<link rel="stylesheet" href="../../resources/css/magnific-popup.css">

<!-- Flexslider  -->
<link rel="stylesheet" href="../../resources/css/flexslider.css">
<link href="../../resources/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Theme style  -->
<link rel="stylesheet" href="../../resources/css/style.css">
<link rel="stylesheet" href="../../resources/css/step.css">

</head>
<body>
<div id="page">
    <div id="fh5co-contact" class="fh5co-no-pd-top">
        <div class="container">
            <div class="row animate-box" style="margin-top:-120px">
                <div class="col-md-12 col-md-offset-0 text-center fh5co-heading">
                <img src="../../resources/photos/logo.png" width="100px">
                <p style="text-align:center;font-size:22px">Reunion - 2017</p>
                    <p style="text-align:center;font-size:20px"><span>Bangladesh Railway Govt. High School, Saltgola, Chittagong.</span></p>
                    
                </div>
            </div>		
            <div class="row">
                <div class="col-md-12 col-md-offset-0 text-center">
                    <div class="form-group row" style="margin-top:-70px">
                        <div class="col-md-12 field">
                                <img src="../../resources/photos/$oneData->picture" width="160px" class="img-rounded img-responsive">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                        <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td style="width: 50%">Reference ID</td>
                                
                                <td style="width: 50%">$oneData->id</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Your Name</td>
                               
                                <td style="width: 50%">$oneData->user_name</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Gender</td>
                               
                                <td style="width: 50%">$oneData->gender</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Father/Husband's Name</td>
                               
                                <td style="width: 50%">$oneData->f_h_name</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Mother's Name</td>
                               
                                <td style="width: 50%">$oneData->mother_name</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Address</td>
                               
                                <td style="width: 50%">$oneData->address</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Profession</td>
                               
                                <td style="width: 50%">$oneData->profession</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Contact No</td>
                               
                                <td style="width: 50%">$oneData->contact</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Passing Year</td>
                               
                                <td style="width: 50%">$oneData->passing_year</td>
                            </tr>
                            <tr>
                            <td style="width: 50%">Religion</td>
                            
                            <td style="width: 50%">$oneData->religion</td>
                            </tr>
                            <tr>
                                <td style="width: 50%">Total Registered Person</td>
                               
                                <td style="width: 50%">$infoDetails</td>
                            </tr>
                            
                            <tr>
                                <td style="width: 50%">Total Amount</td>
                                
                                <td style="width: 50%">$total_fee</td>
                            </tr>
                        </tbody>
                    </table>
                   <h3 style="text-align:center;color:red">$regStatus</h3>
                 <p style="font-size: 16px;text-align:center;> Please contact with us for any information</p>
                <p style="font-size: 16px;text-align:center;>Contact No:+88-01619-820421, +88-01675-920712, +88-01994-104041</p>
             
                </div>
                <hr>
               <p style="font-size: 16px;text-align:center; ">Technical Support: Future Features of IT. 156 CDA Avenue, East Nasirabad, Chittagong. website: www.ffibd.com</p>
            </div>
        </div>
    </div>
    
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
</body>
</html>
EOD;


$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output("$oneData->id"."-".$oneData->user_name.".pdf",'D');