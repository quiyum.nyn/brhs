<?php
    $base_url=base_url;
    $index=$base_url.'views/admin/adminIndex.php';
$memberRequest=$base_url.'views/admin/memberRequest.php';
$reg=$base_url.'views/admin/manualRegistration.php';
$registered=$base_url.'views/admin/registeredMember.php';
$registeredDetails=$base_url.'views/admin/registerdMemberDetails.php';
$reqDetails=$base_url.'views/admin/memberProfile.php';
$reject=$base_url.'views/admin/rejectedMember.php';
$rejectDetails=$base_url.'views/admin/rejectedMemberDetails.php';
$url ='http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
if($url==$memberRequest){
    $memClass="active";
}
elseif($url==$reqDetails){
    $memClass="active";
}
elseif($url==$registered){
    $regClass="active";
}
elseif($url==$registeredDetails){
    $regClass="active";
}
elseif($url==$reject){
    $rejClass="active";
}
elseif($url==$rejectDetails){
    $rejClass="active";
}
elseif($url==$reg){
    $mRegClass="active";
}
?>
<nav class="fh5co-nav" role="navigation">
    <div class="container-fluid">
        <div class="row">
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7 text-left menu-1">
                            <ul>
                                 <li class="<?php echo $memClass?>"><a href="<?php echo base_url;?>views/admin/memberRequest.php"> Request</a></li>
                                 <li class="<?php echo $mRegClass?>"><a href="<?php echo base_url;?>views/admin/manualRegistration.php">Manual Registration</a></li>
                                <li class="<?php echo $regClass?>"><a href="<?php echo base_url;?>views/admin/registeredMember.php">Registered Members</a></li>
                                <li class="<?php echo $rejClass?>"><a href="<?php echo base_url;?>views/admin/rejectedMember.php">Rejected Request</a></li>
                                <li><a href="<?php echo base_url;?>controller/authentication/logout.php">Log Out</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-twitter-with-circle"></i></a></li>
                                <li><a href="#"><i class="icon-facebook-with-circle"></i></a></li>
                                <li><a href="#"><i class="icon-linkedin-with-circle"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble-with-circle"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center menu-2">
                <div id="fh5co-logo">
                    <h1>
                        <a href="<?php echo base_url;?>views/user/index.php">
                            Reunion<span>.</span>
                            <small>2017</small>
                        </a>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</nav>