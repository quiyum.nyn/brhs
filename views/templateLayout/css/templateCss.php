<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="quiyum"/>
<!-- Facebook and Twitter integration -->
<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />

<link href="https://fonts.googleapis.com/css?family=Josefin+Slab:400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
<link rel="icon" href="<?php echo base_url;?>resources/logo.png" type="image/x-icon">

<!-- Animate.css -->
<link rel="stylesheet" href="<?php echo base_url;?>resources/css/animate.css">

<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="<?php echo base_url;?>resources/css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="<?php echo base_url;?>resources/css/bootstrap.css">

<!-- Magnific Popup -->
<link rel="stylesheet" href="<?php echo base_url;?>resources/css/magnific-popup.css">

<!-- Flexslider  -->
<link rel="stylesheet" href="<?php echo base_url;?>resources/css/flexslider.css">
<link href="<?php echo base_url; ?>resources/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Theme style  -->
<link rel="stylesheet" href="<?php echo base_url;?>resources/css/style.css">
<link rel="stylesheet" href="<?php echo base_url;?>resources/css/step.css">
<!-- Modernizr JS -->
<script src="<?php echo base_url;?>resources/js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="<?php echo base_url;?>resources/js/respond.min.js"></script>


<![endif]-->
