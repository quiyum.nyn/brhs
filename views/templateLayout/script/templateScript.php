<!-- jQuery -->
<script src="<?php echo base_url;?>resources/js/jquery-1.11.2.min.js"></script>
<!-- jQuery Easing -->
<script src="<?php echo base_url;?>resources/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url;?>resources/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?php echo base_url;?>resources/js/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="<?php echo base_url;?>resources/js/jquery.flexslider-min.js"></script>
<!-- Magnific Popup -->
<script src="<?php echo base_url;?>resources/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url;?>resources/js/magnific-popup-options.js"></script>
<!-- Main -->
<script src="<?php echo base_url;?>resources/js/main.js"></script>

<script>
    $(document).ready(function() {
        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap"); //Fields wrapper
        var add_button      = $("#add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="col-md-7 col-sm-7 col-xs-7 field"><input type="text" name="child_name[]" class="form-control"></div><div class="col-md-3 col-sm-3 col-xs-3 field"><input type="number" name="child_age[]" min="5" class="form-control"></div><a href=\"#"\ class=\"remove_field btn btn-danger col-md-1 col-lg-1 col-sm-1 col-xs-1"\><i class=\"fa fa-times"\></i></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>
<script>
    $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function(){
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>
<script>
    jQuery(function($) {
        $('#message').fadeOut (1550);
        $('#message').fadeIn (1550);
        $('#message').fadeOut (50);

    })
</script>
