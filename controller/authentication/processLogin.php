<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\Authentication;
use App\Utility\Utility;
use App\Message\Message;
$auth= new Authentication();

$admin= $auth->setData($_POST)->admin();

if($admin){
    $_SESSION['role_status']=0;
    $_SESSION['email']=$_POST['email'];
    Message::setMessage("Success! You have log in Successfully!");
    Utility::redirect('../../views/admin/memberRequest.php');
}
else{
    Message::setMessage("Email and password doesn't match!");
    Utility::redirect('../../views/user/login.php');

}


